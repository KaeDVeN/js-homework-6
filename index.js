"user strict"
//-------1------

const product = {
    name: "bread",
    price: 28,
    discount: 15,
    getTotalPrice (){
        console.log(`Bread вартує ${this.price} гривень, зі знижкою ${this.price * (1 - this.discount / 100)} гривень`)
    }
}

product.getTotalPrice();

//-------2------

// const user = {
//     name: null,
//     age: null,
//     getSayHello(){
//         let userName = prompt("Введіть своє ім'я");
//         let userAge = prompt("Введіть свій вік");
//         user.name = userName;
//         user.age = userAge;
//         return (`Привіт, мене звуть ${user.name} і мені ${user.age} років`);
//     }
// }
//
// alert(user.getSayHello());

//-------3------

// const usersInfo = {
//     Dmytro:{
//         age: 19,
//         hobby: 'reading',
//         address:{
//             country: 'Ukraine',
//             city: 'Ternopil',
//         },
//     },
//     Max:{
//         age: 24,
//         hobby: 'drawing',
//         address:{
//             country: 'Ukraine',
//             city: 'Kyiv',
//         },
//     },
//     Maria:{
//         age: 21,
//         hobby: 'music',
//         address:{
//             country: 'USA',
//             city: 'New York',
//         },
//     },
// }
//
// console.log(usersInfo);
//
// function deepClone(usersInfo) {
//     if (typeof usersInfo !== 'object' || usersInfo === null) {
//         return usersInfo;
//     }
//
//     const clone = {};
//
//     for (let key in usersInfo) {
//         clone[key] = deepClone(usersInfo[key]);
//     }
//
//     return clone;
// }
// const clonedUsersInfo = deepClone(usersInfo);
// console.log(clonedUsersInfo);